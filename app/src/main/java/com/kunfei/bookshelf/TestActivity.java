package com.kunfei.bookshelf;

import android.os.Bundle;

import com.smarx.notchlib.NotchScreenManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class TestActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_info_detail_test);
    }
}
