package com.kunfei.bookshelf.presenter.contract;

import com.kunfei.basemvplib.impl.IPresenter;
import com.kunfei.basemvplib.impl.IView;
import com.kunfei.bookshelf.bean.CategoryListBean;

public interface TopCategoryListContract
{
    interface Presenter extends IPresenter
    {
        void getCategoryList();
    }

    interface View extends IView
    {
        void showCategoryList(CategoryListBean data);
    }
}
