package com.kunfei.bookshelf.help;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kunfei.bookshelf.base.BaseModelImpl;
import com.kunfei.bookshelf.model.analyzeRule.AnalyzeHeaders;
import com.kunfei.bookshelf.model.impl.IHttpGetApi;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class AutoCompleteHelpter
{
    public static AutoCompleteHelpter autoCompleteHelpter;

    public static AutoCompleteHelpter getInstance()
    {
        if (autoCompleteHelpter == null)
        {
            synchronized (AutoCompleteHelpter.class)
            {
                if (autoCompleteHelpter == null)
                {
                    autoCompleteHelpter = new AutoCompleteHelpter();
                }
            }
        }
        return autoCompleteHelpter;
    }

    public Observable<List<String>> getAutoCompleteWords(String key)
    {
        return BaseModelImpl.getInstance().getRetrofitString("http://api.zhuishushenqi.com")
                .create(IHttpGetApi.class)
                .get(String.format("http://api.zhuishushenqi.com/book/auto-complete?query=%s", key),
                        AnalyzeHeaders.getMap(null))
                .flatMap(response -> analyzeAutoCompleteWords(response.body()));
    }

    public Observable<List<String>> analyzeAutoCompleteWords(final String jsonStr)
    {
        return Observable.create(e -> {
            List<String> hotWords = new ArrayList<>();
            JsonObject jsonObject = new JsonParser().parse(jsonStr).getAsJsonObject();
            if (jsonObject.has("ok") && jsonObject.get("ok").getAsBoolean())
            {
                if (jsonObject.has("keywords"))
                {
                    JsonArray jsonArray = jsonObject.getAsJsonArray("keywords");
                    for (int i = 0; i < jsonArray.size(); i++)
                    {
                        hotWords.add(jsonArray.get(i).getAsString());
                    }
                }
            }
            e.onNext(hotWords);
            e.onComplete();
        });
    }
}
