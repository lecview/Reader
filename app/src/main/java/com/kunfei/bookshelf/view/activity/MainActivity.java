//Copyright (c) 2017. 章钦豪. All rights reserved.
package com.kunfei.bookshelf.view.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import com.hwangjr.rxbus.RxBus;
import com.kunfei.bookshelf.BuildConfig;
import com.kunfei.bookshelf.DbHelper;
import com.kunfei.bookshelf.MApplication;
import com.kunfei.bookshelf.R;
import com.kunfei.bookshelf.base.MBaseActivity;
import com.kunfei.bookshelf.base.observer.MyObserver;
import com.kunfei.bookshelf.bean.BookSourceBean;
import com.kunfei.bookshelf.constant.AppConstant;
import com.kunfei.bookshelf.constant.RxBusTag;
import com.kunfei.bookshelf.help.ProcessTextHelp;
import com.kunfei.bookshelf.model.BookSourceManager;
import com.kunfei.bookshelf.model.UpLastChapterModel;
import com.kunfei.bookshelf.presenter.MainPresenter;
import com.kunfei.bookshelf.presenter.contract.MainContract;
import com.kunfei.bookshelf.utils.PermissionUtils;
import com.kunfei.bookshelf.utils.StringUtils;
import com.kunfei.bookshelf.utils.theme.ATH;
import com.kunfei.bookshelf.utils.theme.NavigationViewUtil;
import com.kunfei.bookshelf.utils.theme.ThemeStore;
import com.kunfei.bookshelf.view.fragment.BookListFragment;
import com.kunfei.bookshelf.widget.modialog.InputDialog;
import com.kunfei.bookshelf.widget.modialog.MoDialogHUD;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

import static com.kunfei.bookshelf.constant.AppConstant.DEFAULT_QQ_GROUP_KEY;
import static com.kunfei.bookshelf.constant.AppConstant.KEY_JOIN_QQ_GROUP;
import static com.kunfei.bookshelf.utils.NetworkUtils.isNetWorkAvailable;

public class MainActivity extends MBaseActivity<MainContract.Presenter> implements MainContract.View, BookListFragment.CallbackValue {
    private static final int BACKUP_RESULT = 11;
    private static final int RESTORE_RESULT = 12;
    private static final int FILE_SELECT_RESULT = 13;
    private final int requestSource = 14;

    @BindView(R.id.drawer)
    DrawerLayout drawer;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_view)
    CoordinatorLayout mainView;
    @BindView(R.id.card_search)
    CardView cardSearch;
    @BindView(R.id.tab_vp)
    protected ViewPager mVp;

    private List<Fragment> mFragmentList;
    private TabFragmentPageAdapter mTabFragmentPageAdapter;
    private int group;
    private boolean viewIsList;
    private ActionBarDrawerToggle mDrawerToggle;
    private MoDialogHUD moDialogHUD;
    private long exitTime = 0;
    private boolean resumed = false;
    private Handler handler = new Handler();


    public final SharedPreferences preferences = MApplication.getConfigPreferences();

    @Override
    protected MainContract.Presenter initInjector() {
        return new MainPresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            resumed = savedInstanceState.getBoolean("resumed");
        }
        group = preferences.getInt("bookshelfGroup", 0);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("resumed", resumed);
    }

    @Override
    protected void onCreateActivity() {
        getWindow().getDecorView().setBackgroundColor(ThemeStore.backgroundColor(this));
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        String shared_url = preferences.getString("shared_url", "");
        assert shared_url != null;
        if (shared_url.length() > 1) {
            InputDialog.builder(this)
                    .setTitle(getString(R.string.add_book_url))
                    .setDefaultValue(shared_url)
                    .setCallback(inputText -> {
                        inputText = StringUtils.trim(inputText);
                        mPresenter.addBookUrl(inputText);
                    }).show();
            preferences.edit()
                    .putString("shared_url", "")
                    .apply();
        }
    }

    /**
     * 沉浸状态栏
     */
    @Override
    public void initImmersionBar() {
        super.initImmersionBar();
    }

    @Override
    protected void initData() {
        viewIsList = preferences.getBoolean("bookshelfIsList", true);
        mFragmentList = createFragments();

        if (!preferences.getBoolean(AppConstant.KEY_ALLOWOPENAPP, true)) {
            finish();
        }
    }

    @Override
    public boolean isRecreate() {
        return isRecreate;
    }

    @Override
    public int getGroup() {
        return group;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }

    public List<Fragment> createFragments() {
        BookListFragment bookListFragment = null;
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof BookListFragment) {
                bookListFragment = (BookListFragment) fragment;
            }
        }
        if (bookListFragment == null)
            bookListFragment = new BookListFragment();
        return Arrays.asList(bookListFragment);
    }

    @Override
    protected void bindView() {
        super.bindView();
        setSupportActionBar(toolbar);
        setupActionBar();
        cardSearch.setCardBackgroundColor(ThemeStore.primaryColorDark(this));
        initDrawer();
        initViewPager();
        moDialogHUD = new MoDialogHUD(this);
        if (!preferences.getBoolean("behaviorMain", true)) {
            AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
            params.setScrollFlags(0);
        }
        //点击跳转搜索页
        cardSearch.setOnClickListener(view -> startSearchActivity());
    }

    public void startSearchActivity() {
        startActivity(new Intent(this,
                SearchBookActivity.class));
    }

    class TabFragmentPageAdapter extends FragmentPagerAdapter {

        TabFragmentPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }

    //初始化TabLayout和ViewPager
    private void initViewPager() {
        mTabFragmentPageAdapter = new TabFragmentPageAdapter(getSupportFragmentManager());
        mVp.setAdapter(mTabFragmentPageAdapter);
        mVp.setOffscreenPageLimit(1);
    }

    public ViewPager getViewPager() {
        return mVp;
    }

    public BookListFragment getBookListFragment() {
        try {
            return (BookListFragment) createFragments().get(0);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // 这个必须要，没有的话进去的默认是个箭头。。正常应该是三横杠的
        mDrawerToggle.syncState();
    }

    /**
     * 菜单事件
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        SharedPreferences.Editor editor = preferences.edit();
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                if (drawer.isDrawerOpen(GravityCompat.START)
                ) {
                    drawer.closeDrawers();
                } else {
                    drawer.openDrawer(GravityCompat.START, !MApplication.isEInkMode);
                }
                break;
            case R.id.action_bookrack:
                handler.postDelayed(() -> TopCategoryListActivity.startThis(this), 200);
                break;
            case R.id.action_add_local:
                PermissionUtils.checkMorePermissions(this, MApplication.PerList, new PermissionUtils.PermissionCheckCallback() {
                    @Override
                    public void onHasPermission() {
                        startActivity(new Intent(MainActivity.this, ImportBookActivity.class));
                    }

                    @Override
                    public void onUserHasAlreadyTurnedDown(String... permission) {
                        MainActivity.this.toast(R.string.import_per);
                    }

                    @Override
                    public void onAlreadyTurnedDownAndNoAsk(String... permission) {
                        MainActivity.this.toast(R.string.please_grant_storage_permission);
                        PermissionUtils.requestMorePermissions(MainActivity.this, permission, FILE_SELECT_RESULT);
                    }
                });
                break;
            case R.id.action_download_all:
                if (!isNetWorkAvailable()) {
                    toast(R.string.network_connection_unavailable);
                } else {
                    RxBus.get().post(RxBusTag.DOWNLOAD_ALL, 10000);
                }
                break;
            case R.id.action_book_source_manage:
                handler.postDelayed(() -> BookSourceActivity.startThis(this, requestSource), 200);
                break;
            case R.id.action_booksource_update: {
                try {
                    InputStream inputStream = getAssets().open("myBookSource.json");
                    InputStreamReader inputStreamReader = null;
                    try {
                        inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                    BufferedReader reader = new BufferedReader(inputStreamReader);
                    StringBuffer sb = new StringBuffer("");
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    Observable<List<BookSourceBean>> observable = BookSourceManager.importSource(sb.toString());
                    if (observable != null) {
                        observable.subscribe(new MyObserver<List<BookSourceBean>>() {
                            @SuppressLint("DefaultLocale")
                            @Override
                            public void onNext(List<BookSourceBean> bookSourceBeans) {
                                handler.post(() -> Toast.makeText(getApplicationContext(),
                                        R.string.tips_booksource_already_update,
                                        Toast.LENGTH_SHORT).show());
                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case R.id.action_software_share:
                shareSoftWare();
                break;
            case R.id.action_join_qq_group:
                joinQQGroup(preferences
                        .getString(KEY_JOIN_QQ_GROUP, DEFAULT_QQ_GROUP_KEY));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean joinQQGroup(String key) {
        Intent intent = new Intent();
        intent.setData(Uri.parse("mqqopensdkapi://bizAgent/qm/qr?url=http%3A%2F%2Fqm.qq.com%2Fcgi-bin%2Fqm%2Fqr%3Ffrom%3Dapp%26p%3Dandroid%26k%3D" + key));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            startActivity(intent);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void shareSoftWare() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("软件分享");
        builder.setMessage("分享是件很酷的事情，快来分享给小伙伴们一起使用吧！");
        builder.setPositiveButton("分享", (dialogInterface, i) -> {
            Context ctx = getApplicationContext();
            PackageManager pm = ctx.getPackageManager();
            try {
                String authority = ctx.getPackageName() + ".fileprovider";
                File apkFile = new File(pm.getApplicationInfo(ctx.getPackageName(), 0).sourceDir);
                Uri uri = Uri.fromFile(apkFile);
                Intent intent = new Intent(Intent.ACTION_SEND);
                String ext = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(ext);
                if (mimeType == null)
                    mimeType = "*/*";
                intent.setType(mimeType);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(MainActivity.this, authority, apkFile);
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                intent.putExtra(Intent.EXTRA_STREAM, uri);
                startActivity(Intent.createChooser(intent, getResources().getString(R.string.share_app)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).setNegativeButton("取消", (dialogInterface, i) -> {
        }).setOnDismissListener(dialogInterface -> {
        }).setCancelable(false);
        builder.create();
        builder.show();
    }

    //设置ToolBar
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    //初始化侧边栏
    private void initDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerToggle.syncState();
        drawer.addDrawerListener(mDrawerToggle);

        setUpNavigationView();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * 侧边栏按钮
     */
    private void setUpNavigationView() {
        navigationView.setBackgroundColor(ThemeStore.backgroundColor(this));
        NavigationViewUtil.setItemTextColors(navigationView, getResources().getColor(R.color.tv_text_default), ThemeStore.accentColor(this));
        NavigationViewUtil.setItemIconColors(navigationView, getResources().getColor(R.color.tv_text_default), ThemeStore.accentColor(this));
        NavigationViewUtil.disableScrollbar(navigationView);
        @SuppressLint("InflateParams") View headerView = LayoutInflater.from(this).inflate(R.layout.navigation_header, null);
        navigationView.addHeaderView(headerView);
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            drawer.closeDrawer(GravityCompat.START, !MApplication.isEInkMode);
            switch (menuItem.getItemId()) {
                case R.id.action_bookrack:
                    handler.postDelayed(() -> TopCategoryListActivity.startThis(this), 200);
                    break;
                case R.id.action_book_source_manage:
                    handler.postDelayed(() -> BookSourceActivity.startThis(this, requestSource), 200);
                    break;
                case R.id.action_download:
                    handler.postDelayed(() -> DownloadActivity.startThis(this), 200);
                    break;
                case R.id.action_setting:
                    handler.postDelayed(() -> SettingActivity.startThis(this), 200);
                    break;
                case R.id.action_about:
                    handler.postDelayed(() -> AboutActivity.startThis(this), 200);
                    break;
                case R.id.action_theme:
                    handler.postDelayed(() -> ThemeSettingActivity.startThis(this), 200);
                    break;
            }
            return true;
        });
    }

    /**
     * 备份
     */
    private void backup() {
        PermissionUtils.checkMorePermissions(this, MApplication.PerList, new PermissionUtils.PermissionCheckCallback() {
            @Override
            public void onHasPermission() {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.backup_confirmation)
                        .setMessage(R.string.backup_message)
                        .setPositiveButton(R.string.ok, (dialog, which) -> mPresenter.backupData())
                        .setNegativeButton(R.string.cancel, null)
                        .show();
                ATH.setAlertDialogTint(alertDialog);
            }

            @Override
            public void onUserHasAlreadyTurnedDown(String... permission) {
                MainActivity.this.toast(R.string.backup_permission);
            }

            @Override
            public void onAlreadyTurnedDownAndNoAsk(String... permission) {
                MainActivity.this.toast(R.string.backup_permission);
                PermissionUtils.requestMorePermissions(MainActivity.this, permission, BACKUP_RESULT);
            }
        });
    }

    /**
     * 恢复
     */
    private void restore() {
        PermissionUtils.checkMorePermissions(this, MApplication.PerList, new PermissionUtils.PermissionCheckCallback() {
            @Override
            public void onHasPermission() {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.restore_confirmation)
                        .setMessage(R.string.restore_message)
                        .setPositiveButton(R.string.ok, (dialog, which) -> mPresenter.restoreData())
                        .setNegativeButton(R.string.cancel, null)
                        .show();
                ATH.setAlertDialogTint(alertDialog);
            }

            @Override
            public void onUserHasAlreadyTurnedDown(String... permission) {
                MainActivity.this.toast(R.string.restore_permission);
            }

            @Override
            public void onAlreadyTurnedDownAndNoAsk(String... permission) {
                PermissionUtils.requestMorePermissions(MainActivity.this, permission, RESTORE_RESULT);
            }
        });
    }

    /**
     * 新版本运行
     */
    private void versionUpRun() {
    }

    /**
     * 获取权限
     */
    private void requestPermission() {
        List<String> per = PermissionUtils.checkMorePermissions(this, MApplication.PerList);
        if (per.size() > 0) {
            PermissionUtils.requestMorePermissions(this, per, MApplication.RESULT__PERMS);
        }
    }

    @Override
    protected void firstRequest() {
        if (!isRecreate) {
            versionUpRun();
        }

        requestPermission();
        handler.postDelayed(() -> {
            UpLastChapterModel.getInstance().startUpdate();
            if (BuildConfig.DEBUG) {
                ProcessTextHelp.setProcessTextEnable(false);
            }
        }, 60 * 1000);
    }

    @Override
    public void dismissHUD() {
        moDialogHUD.dismiss();
    }

    public void onRestore(String msg) {
        moDialogHUD.showLoading(msg);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionUtils.checkMorePermissions(this, MApplication.PerList, new PermissionUtils.PermissionCheckCallback() {
            @Override
            public void onHasPermission() {
                switch (requestCode) {
                    case FILE_SELECT_RESULT:
                        startActivity(new Intent(MainActivity.this, ImportBookActivity.class));
                        break;
                    case BACKUP_RESULT:
                        backup();
                        break;
                    case RESTORE_RESULT:
                        restore();
                        break;
                }
            }

            @Override
            public void onUserHasAlreadyTurnedDown(String... permission) {
                switch (requestCode) {
                    case FILE_SELECT_RESULT:
                        MainActivity.this.toast(R.string.import_book_per);
                        break;
                    case BACKUP_RESULT:
                        MainActivity.this.toast(R.string.backup_permission);
                        break;
                    case RESTORE_RESULT:
                        MainActivity.this.toast(R.string.restore_permission);
                        break;
                }
            }

            @Override
            public void onAlreadyTurnedDownAndNoAsk(String... permission) {
                switch (requestCode) {
                    case FILE_SELECT_RESULT:
                        MainActivity.this.toast(R.string.import_book_per);
                        break;
                    case BACKUP_RESULT:
                        MainActivity.this.toast(R.string.backup_permission);
                        break;
                    case RESTORE_RESULT:
                        MainActivity.this.toast(R.string.restore_permission);
                        break;
                }
                PermissionUtils.toAppSetting(MainActivity.this);
            }
        });
    }

    @SuppressLint("RtlHardcoded")
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Boolean mo = moDialogHUD.onKeyDown(keyCode, event);
        if (mo) {
            return true;
        } else {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START, !MApplication.isEInkMode);
                    return true;
                }
                exit();
                return true;
            }
            return super.onKeyDown(keyCode, event);
        }
    }

    /**
     * 退出
     */
    public void exit() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            showSnackBar(toolbar, getString(R.string.double_click_exit));
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }

    @Override
    public void recreate() {
        super.recreate();
    }

    @Override
    protected void onDestroy() {
        UpLastChapterModel.destroy();
        DbHelper.getDaoSession().getBookContentBeanDao().deleteAll();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == requestSource) {

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
