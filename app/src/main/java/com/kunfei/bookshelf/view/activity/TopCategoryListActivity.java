package com.kunfei.bookshelf.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;

import com.kunfei.bookshelf.R;
import com.kunfei.bookshelf.base.MBaseActivity;
import com.kunfei.bookshelf.bean.CategoryListBean;
import com.kunfei.bookshelf.constant.AppConstant;
import com.kunfei.bookshelf.presenter.TopCategoryListPresenter;
import com.kunfei.bookshelf.presenter.contract.TopCategoryListContract;
import com.kunfei.bookshelf.utils.theme.ThemeStore;
import com.kunfei.bookshelf.view.adapter.TopCategoryListAdapter;
import com.kunfei.bookshelf.widget.SupportGridItemDecoration;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TopCategoryListActivity extends MBaseActivity<TopCategoryListContract.Presenter>
        implements TopCategoryListContract.View
{
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvMaleCategory)
    RecyclerView mRvMaleCategory;
    @BindView(R.id.rvFemaleCategory)
    RecyclerView mRvFeMaleCategory;

    private TopCategoryListAdapter mMaleCategoryListAdapter;
    private TopCategoryListAdapter mFemaleCategoryListAdapter;
    private List<CategoryListBean.MaleBean> mMaleCategoryList = new ArrayList<>();
    private List<CategoryListBean.MaleBean> mFemaleCategoryList = new ArrayList<>();

    @Override
    protected TopCategoryListContract.Presenter initInjector()
    {
        return new TopCategoryListPresenter();
    }

    @Override
    protected void onCreateActivity()
    {
        getWindow().getDecorView().setBackgroundColor(ThemeStore.backgroundColor(this));
        setContentView(R.layout.activity_top_category_list);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch (id)
        {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void initData()
    {
        ButterKnife.bind(this);
        this.setSupportActionBar(toolbar);
        setupActionBar();
    }

    class ClickListener implements TopCategoryListAdapter.OnRvItemClickListener<CategoryListBean.MaleBean>
    {
        private String gender;

        public ClickListener(@AppConstant.Gender String gender)
        {
            this.gender = gender;
        }

        @Override
        public void onItemClick(View view, int position, CategoryListBean.MaleBean data)
        {
            SubCategoryListActivity.startThis(TopCategoryListActivity.this, data.name, gender);
        }
    }

    @Override
    protected void bindView()
    {
        mRvMaleCategory.setHasFixedSize(true);
        mRvMaleCategory.setLayoutManager(new GridLayoutManager(this, 3));
        mRvMaleCategory.addItemDecoration(new SupportGridItemDecoration(this));
        mRvFeMaleCategory.setHasFixedSize(true);
        mRvFeMaleCategory.setLayoutManager(new GridLayoutManager(this, 3));
        mRvFeMaleCategory.addItemDecoration(new SupportGridItemDecoration(this));
        mMaleCategoryListAdapter = new TopCategoryListAdapter(this, mMaleCategoryList, new ClickListener(AppConstant.Gender.MALE));
        mFemaleCategoryListAdapter = new TopCategoryListAdapter(this, mFemaleCategoryList, new ClickListener(AppConstant.Gender.FEMALE));
        mRvMaleCategory.setAdapter(mMaleCategoryListAdapter);
        mRvFeMaleCategory.setAdapter(mFemaleCategoryListAdapter);
    }

    @Override
    protected void firstRequest()
    {
        mPresenter.getCategoryList();
    }

    //设置ToolBar
    private void setupActionBar()
    {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.category);
        }
    }

    @Override
    public void showCategoryList(CategoryListBean data)
    {
        mMaleCategoryList.clear();
        mFemaleCategoryList.clear();
        mMaleCategoryList.addAll(data.male);
        mFemaleCategoryList.addAll(data.female);
        mMaleCategoryListAdapter.notifyDataSetChanged();
        mFemaleCategoryListAdapter.notifyDataSetChanged();
    }

    public static void startThis(Activity activity)
    {
        Intent intent = new Intent(activity, TopCategoryListActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public boolean isSupportSwipeBack()
    {
        return true;
    }
}
